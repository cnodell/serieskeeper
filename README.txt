============
Serieskeeper
============

Overview
========

Serieskeeper (sk), records the series you have started reading so that you
will not forget about them at a later date. This is a bare-bones command line
tool written in Python.

Basic Install
=============

Use ``setup.py``::

   $ wget https://bitbucket.org/cnodell/serieskeeper/downloads/serieskeeper-vX.X.X.tar.gz
   $ tar xvfz serieskeeper-vX.X.X.tar.gz
   $ cd serieskeeper-vX.X.X
   $ [sudo] python setup.py install

Program Usage
=============

Usage documentation can be found at: 
https://bitbucket.org/cnodell/serieskeeper/wiki/Home

More Information
================

See https://bitbucket.org/cnodell/serieskeeper/overview for more information.
