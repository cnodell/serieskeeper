from nose.tools import *
from serieskeeper.data import *
import os
import cPickle

def setup():
    print "SETUP!"

def teardown():
    print "TEAR DOWN!"


def test_create_collection():
    file_path ='collection'
    if os.path.exists(file_path):
        os.remove(file_path)
    create_collection(file_path)
    f = open(file_path, 'rb')
    collection = cPickle.load(f)
    f.close()
    assert_equal(collection, [])
    assert_raises(ConflictError, create_collection, (file_path))
    os.remove(file_path)

def test_get_collection():
    file_path ='collection'
    if os.path.exists(file_path):
        os.remove(file_path)
    assert_raises(IOError, get_collection, (file_path))
    f = open(file_path, 'wb')
    f.close()
    assert_raises(EOFError, get_collection, (file_path))
    os.remove(file_path)
    f = open(file_path, 'wb')
    cPickle.dump([], f)
    f.close()
    collection = get_collection(file_path)
    assert_equal([], collection)

